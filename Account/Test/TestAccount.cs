﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using OOP18;

namespace Test
{
    [TestClass]
    public class TestAccount
    {
        private const string DEFAULT_USERNAME = "prova";
        private const string DEFAULT_PASSWORD = "prova";
        private IAccount account;

        [TestMethod]
        public void TestGettersAccount()
        {
            account = new Account.Builder(DEFAULT_USERNAME, DEFAULT_PASSWORD).Build();
            AreEqual(DEFAULT_USERNAME, account.Username);
            AreEqual(DEFAULT_PASSWORD, account.Password);
            AreEqual(DEFAULT_USERNAME, account.Nickname);
            AreEqual(0, account.BestScore);
        }

        [TestMethod]
        public void TestSettersAccount()
        {
            account = new Account.Builder(DEFAULT_USERNAME, DEFAULT_PASSWORD).Build();
            AreEqual(DEFAULT_USERNAME, account.Username);
            const string NEW_PASSWORD = "test";
            this.account.Password = NEW_PASSWORD;
            AreNotEqual(DEFAULT_PASSWORD, account.Password);
            AreEqual(NEW_PASSWORD, account.Password);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestNullArgumentException()
        {
            account = new Account.Builder(DEFAULT_USERNAME, null)
                                 .Build();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestArgumentException()
        {
            account = new Account.Builder(DEFAULT_USERNAME, DEFAULT_PASSWORD)
                                 .BestScore(-10)
                                 .Build();
        }

        [TestMethod]
        public void TestEqualsAndHashCode()
        {
            account = new Account.Builder(DEFAULT_USERNAME, DEFAULT_PASSWORD).Build();
            IAccount accountEqual = new Account.Builder(DEFAULT_USERNAME, DEFAULT_PASSWORD).Build();
            IsTrue(account.Equals(accountEqual));
        }
    }
}
