﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using static Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using OOP18;
using System.Collections.Generic;

namespace Test
{
    [TestClass]
    public class TestAccountManager
    {
        private static readonly string USERNAME_1 = "prova";
        private static readonly string USERNAME_2 = "test";
        private static readonly string PASSWORD = "prova";
        private static readonly string PASSWORD_2 = "test";
        private static readonly IAccount ACCOUNT_1 = new Account.Builder(USERNAME_1, PASSWORD).Build();
        private static readonly IAccount ACCOUNT_2 = new Account.Builder(USERNAME_2, PASSWORD).Build();
        private static ISet<IAccount> ACCOUNTS = new HashSet<IAccount>();
        private IAccountManager accountManager;

        public TestAccountManager()
        {
            ACCOUNTS.Add(ACCOUNT_1);
        }

        [TestMethod]
        public void TestIsPresent()
        {
            this.accountManager = new AccountManager(ACCOUNTS);
            IsTrue(this.accountManager.IsPresent(ACCOUNT_1));
            IsFalse(this.accountManager.IsPresent(ACCOUNT_2));
        }

        [TestMethod]
        public void TestRegister()
        {
            this.accountManager = new AccountManager(ACCOUNTS);
            this.accountManager.Register(ACCOUNT_2);
            IsTrue(this.accountManager.IsPresent(ACCOUNT_2));
        }

        [TestMethod]
        public void TestCheckPassword()
        {
            this.accountManager = new AccountManager(ACCOUNTS);
            IsTrue(this.accountManager.CheckPassword(new Account.Builder(USERNAME_1, PASSWORD).Build()));
            IsFalse(this.accountManager.CheckPassword(new Account.Builder(USERNAME_1, PASSWORD_2).Build()));
        }
    }
}
