﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOP18
{
    public partial class Register : Form
    {
        private readonly IAccountManager accountManager;

        public Register()
        {
            this.accountManager = new AccountManager(FileUtils.GetAccounts());
            InitializeComponent();
        }

        private void Sign(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.pswTextBox.Text) || string.IsNullOrEmpty(this.confPswTextBox.Text))
            {
                MessageBox.Show("Le password sono invalide", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (this.pswTextBox.Text != this.confPswTextBox.Text)
            {
                MessageBox.Show("Le password non corrispondono", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (string.IsNullOrEmpty(this.usrTextBox.Text))
            {
                MessageBox.Show("Il nome utente non può essere vuoto", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                IAccount account = new Account.Builder(this.usrTextBox.Text, this.pswTextBox.Text).Build();
                if (this.accountManager.IsPresent(account))
                {
                    MessageBox.Show("L'account già esiste", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    IAccount accountComplete = new Account.Builder(this.usrTextBox.Text, this.pswTextBox.Text)
                                                          .WithNickname(this.nickTextBox.Text)
                                                          .Build();
                    this.accountManager.Register(accountComplete);
                    FileUtils.PrintAccount(accountComplete);
                    if (MessageBox.Show("Registrazione effettuata", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information) == DialogResult.OK)
                    {
                        Close();
                    }
                }
            }
        }

        private void Esc(object sender, EventArgs e) => Close();

        private void ShowPassword(object sender, EventArgs e) => this.pswTextBox.UseSystemPasswordChar = !this.pswTextBox.UseSystemPasswordChar;

        private void ShowConfPassword(object sender, EventArgs e) => this.confPswTextBox.UseSystemPasswordChar = !this.confPswTextBox.UseSystemPasswordChar;
        
    }
}
