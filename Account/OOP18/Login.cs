﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOP18
{
    public partial class Login : Form
    {
        private readonly IAccountManager accountManager;

        public Login()
        {
            this.accountManager = new AccountManager(FileUtils.GetAccounts());
            InitializeComponent();
        }

        private void TryLogin(object sender, EventArgs e)
        {
            IAccount account = new Account.Builder(this.usrTextBox.Text, this.pswTextBox.Text).Build();
            if (!this.accountManager.IsPresent(account))
            {
                MessageBox.Show("Il tuo nome utente non esiste", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (!this.accountManager.CheckPassword(account))
            {
                MessageBox.Show("La password è errata", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Login effettuato", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Environment.Exit(0);
            }
        }

        private void Register(object sender, EventArgs e) => new Register().ShowDialog();

        private void Exit(object sender, EventArgs e) => Environment.Exit(0);
        
    }
}
