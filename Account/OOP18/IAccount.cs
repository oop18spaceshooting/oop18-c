﻿namespace OOP18
{
    public interface IAccount
    {
        int BestScore { get; set; }
        string Nickname { get; set; }
        string Password { get; set; }
        string Username { get; }
    }
}