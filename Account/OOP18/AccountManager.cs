﻿using System.Collections.Generic;
using System.Linq;

namespace OOP18
{
    public class AccountManager : IAccountManager
    {
        private readonly ISet<IAccount> accounts;

        public AccountManager(IEnumerable<IAccount> accounts) => this.accounts = new HashSet<IAccount>(accounts);

        public bool CheckPassword(IAccount account)
        {
            IAccount realAccount = this.accounts.Where(a => a.Equals(account)).First();
            return realAccount != null ? realAccount.Password == account.Password : false;
        }

        public bool IsPresent(IAccount account) => accounts.Contains(account);

        public void Register(IAccount account) => accounts.Add(account);
    }
}
