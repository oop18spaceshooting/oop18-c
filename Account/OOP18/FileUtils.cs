﻿using System;
using System.Collections.Generic;
using System.IO;

namespace OOP18
{
    public static class FileUtils
    {
        private static readonly string ACCOUNT_PATH = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + Path.DirectorySeparatorChar + "accountsC#";
        public static IEnumerable<IAccount> GetAccounts()
        {
            Directory.CreateDirectory(ACCOUNT_PATH);
            string[] files = Directory.GetFiles(ACCOUNT_PATH);
            foreach (string file in files)
            {
                var lines = File.ReadLines(file).GetEnumerator();
                lines.MoveNext();
                string username = lines.Current;
                lines.MoveNext();
                string password = lines.Current;
                lines.MoveNext();
                string nickname = lines.Current;
                lines.MoveNext();
                int bestScore = int.Parse(lines.Current);
                if (lines.MoveNext())
                {
                    throw new IOException();
                }
                yield return new Account.Builder(username, password)
                                        .WithNickname(nickname)
                                        .BestScore(bestScore)
                                        .Build();
            }
        }
 
        public static void PrintAccount(IAccount account)
        {
            string[] lines = { account.Username, account.Password, account.Nickname, account.BestScore.ToString()};
            File.WriteAllLines(ACCOUNT_PATH + Path.DirectorySeparatorChar + account.Username + ".txt", lines);
        }
    }
}

    
