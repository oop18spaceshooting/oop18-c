﻿namespace OOP18
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.loginBtn = new System.Windows.Forms.Button();
            this.regBtn = new System.Windows.Forms.Button();
            this.escBtn = new System.Windows.Forms.Button();
            this.usrLabel = new System.Windows.Forms.Label();
            this.pswLabel = new System.Windows.Forms.Label();
            this.title = new System.Windows.Forms.Label();
            this.usrTextBox = new System.Windows.Forms.TextBox();
            this.pswTextBox = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tableLayoutPanel.AutoScroll = true;
            this.tableLayoutPanel.ColumnCount = 3;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 52.47525F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 47.52475F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tableLayoutPanel.Controls.Add(this.loginBtn, 1, 3);
            this.tableLayoutPanel.Controls.Add(this.regBtn, 1, 4);
            this.tableLayoutPanel.Controls.Add(this.escBtn, 2, 4);
            this.tableLayoutPanel.Controls.Add(this.usrLabel, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.pswLabel, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.title, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.usrTextBox, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.pswTextBox, 1, 2);
            this.tableLayoutPanel.Location = new System.Drawing.Point(-1, -1);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 5;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 48.53801F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 51.46199F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 64F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 73F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 68F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(476, 335);
            this.tableLayoutPanel.TabIndex = 7;
            // 
            // loginBtn
            // 
            this.loginBtn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.loginBtn.Location = new System.Drawing.Point(202, 218);
            this.loginBtn.Name = "loginBtn";
            this.loginBtn.Size = new System.Drawing.Size(75, 23);
            this.loginBtn.TabIndex = 0;
            this.loginBtn.Text = "Accedi";
            this.loginBtn.UseVisualStyleBackColor = true;
            this.loginBtn.Click += new System.EventHandler(this.TryLogin);
            // 
            // regBtn
            // 
            this.regBtn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.regBtn.Location = new System.Drawing.Point(202, 289);
            this.regBtn.Name = "regBtn";
            this.regBtn.Size = new System.Drawing.Size(75, 23);
            this.regBtn.TabIndex = 1;
            this.regBtn.Text = "Registrati";
            this.regBtn.UseVisualStyleBackColor = true;
            this.regBtn.Click += new System.EventHandler(this.Register);
            // 
            // escBtn
            // 
            this.escBtn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.escBtn.Location = new System.Drawing.Point(358, 289);
            this.escBtn.Name = "escBtn";
            this.escBtn.Size = new System.Drawing.Size(75, 23);
            this.escBtn.TabIndex = 3;
            this.escBtn.Text = "Esci";
            this.escBtn.UseVisualStyleBackColor = true;
            this.escBtn.Click += new System.EventHandler(this.Exit);
            // 
            // usrLabel
            // 
            this.usrLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.usrLabel.AutoSize = true;
            this.usrLabel.Location = new System.Drawing.Point(15, 89);
            this.usrLabel.Name = "usrLabel";
            this.usrLabel.Size = new System.Drawing.Size(135, 13);
            this.usrLabel.TabIndex = 4;
            this.usrLabel.Text = "Inserisci il tuo nome utente:";
            // 
            // pswLabel
            // 
            this.pswLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pswLabel.AutoSize = true;
            this.pswLabel.Location = new System.Drawing.Point(20, 154);
            this.pswLabel.Name = "pswLabel";
            this.pswLabel.Size = new System.Drawing.Size(125, 13);
            this.pswLabel.TabIndex = 5;
            this.pswLabel.Text = "Inserisci la tua password:";
            // 
            // title
            // 
            this.title.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.title.AutoSize = true;
            this.title.Location = new System.Drawing.Point(207, 25);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(65, 13);
            this.title.TabIndex = 6;
            this.title.Text = "ACCEDERE";
            this.title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // usrTextBox
            // 
            this.usrTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.usrTextBox.Location = new System.Drawing.Point(190, 86);
            this.usrTextBox.Name = "usrTextBox";
            this.usrTextBox.Size = new System.Drawing.Size(100, 20);
            this.usrTextBox.TabIndex = 7;
            // 
            // pswTextBox
            // 
            this.pswTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pswTextBox.Location = new System.Drawing.Point(190, 151);
            this.pswTextBox.Name = "pswTextBox";
            this.pswTextBox.Size = new System.Drawing.Size(100, 20);
            this.pswTextBox.TabIndex = 8;
            this.pswTextBox.UseSystemPasswordChar = true;
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(474, 332);
            this.Controls.Add(this.tableLayoutPanel);
            this.MinimumSize = new System.Drawing.Size(470, 350);
            this.Name = "Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.Button loginBtn;
        private System.Windows.Forms.Button regBtn;
        private System.Windows.Forms.Button escBtn;
        private System.Windows.Forms.Label usrLabel;
        private System.Windows.Forms.Label pswLabel;
        private System.Windows.Forms.Label title;
        private System.Windows.Forms.TextBox usrTextBox;
        private System.Windows.Forms.TextBox pswTextBox;
    }
}