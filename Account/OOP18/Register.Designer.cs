﻿namespace OOP18
{
    partial class Register
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.regLabel = new System.Windows.Forms.Label();
            this.usrLabel = new System.Windows.Forms.Label();
            this.nickLabel = new System.Windows.Forms.Label();
            this.pswLabel = new System.Windows.Forms.Label();
            this.regBtn = new System.Windows.Forms.Button();
            this.escBtn = new System.Windows.Forms.Button();
            this.confPswLabel = new System.Windows.Forms.Label();
            this.confPswTextBox = new System.Windows.Forms.TextBox();
            this.pswTextBox = new System.Windows.Forms.TextBox();
            this.nickTextBox = new System.Windows.Forms.TextBox();
            this.usrTextBox = new System.Windows.Forms.TextBox();
            this.pswCheckBox = new System.Windows.Forms.CheckBox();
            this.confPswCheckBox = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tableLayoutPanel.ColumnCount = 3;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 47.31935F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 52.68065F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 196F));
            this.tableLayoutPanel.Controls.Add(this.regLabel, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.usrLabel, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.nickLabel, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.pswLabel, 0, 3);
            this.tableLayoutPanel.Controls.Add(this.regBtn, 1, 5);
            this.tableLayoutPanel.Controls.Add(this.escBtn, 2, 5);
            this.tableLayoutPanel.Controls.Add(this.confPswLabel, 0, 4);
            this.tableLayoutPanel.Controls.Add(this.confPswTextBox, 1, 4);
            this.tableLayoutPanel.Controls.Add(this.pswTextBox, 1, 3);
            this.tableLayoutPanel.Controls.Add(this.nickTextBox, 1, 2);
            this.tableLayoutPanel.Controls.Add(this.usrTextBox, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.pswCheckBox, 2, 3);
            this.tableLayoutPanel.Controls.Add(this.confPswCheckBox, 2, 4);
            this.tableLayoutPanel.Location = new System.Drawing.Point(-15, -9);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 6;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40.81633F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 59.18367F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 71F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 71F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(618, 399);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // regLabel
            // 
            this.regLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.regLabel.AutoSize = true;
            this.regLabel.Location = new System.Drawing.Point(262, 19);
            this.regLabel.Name = "regLabel";
            this.regLabel.Size = new System.Drawing.Size(95, 13);
            this.regLabel.TabIndex = 2;
            this.regLabel.Text = "REGISTRAZIONE";
            this.regLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // usrLabel
            // 
            this.usrLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.usrLabel.AutoSize = true;
            this.usrLabel.Location = new System.Drawing.Point(37, 81);
            this.usrLabel.Name = "usrLabel";
            this.usrLabel.Size = new System.Drawing.Size(125, 13);
            this.usrLabel.TabIndex = 3;
            this.usrLabel.Text = "Inserisci un nome utente:";
            // 
            // nickLabel
            // 
            this.nickLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.nickLabel.AutoSize = true;
            this.nickLabel.Location = new System.Drawing.Point(37, 152);
            this.nickLabel.Name = "nickLabel";
            this.nickLabel.Size = new System.Drawing.Size(124, 13);
            this.nickLabel.TabIndex = 4;
            this.nickLabel.Text = "Inserisci un soprannome:";
            // 
            // pswLabel
            // 
            this.pswLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pswLabel.AutoSize = true;
            this.pswLabel.Location = new System.Drawing.Point(41, 223);
            this.pswLabel.Name = "pswLabel";
            this.pswLabel.Size = new System.Drawing.Size(117, 13);
            this.pswLabel.TabIndex = 5;
            this.pswLabel.Text = "Inserisci una password:";
            // 
            // regBtn
            // 
            this.regBtn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.regBtn.Location = new System.Drawing.Point(272, 351);
            this.regBtn.Name = "regBtn";
            this.regBtn.Size = new System.Drawing.Size(75, 23);
            this.regBtn.TabIndex = 0;
            this.regBtn.Text = "Registrati";
            this.regBtn.UseVisualStyleBackColor = true;
            this.regBtn.Click += new System.EventHandler(this.Sign);
            // 
            // escBtn
            // 
            this.escBtn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.escBtn.Location = new System.Drawing.Point(482, 351);
            this.escBtn.Name = "escBtn";
            this.escBtn.Size = new System.Drawing.Size(75, 23);
            this.escBtn.TabIndex = 1;
            this.escBtn.Text = "Indietro";
            this.escBtn.UseVisualStyleBackColor = true;
            this.escBtn.Click += new System.EventHandler(this.Esc);
            // 
            // confPswLabel
            // 
            this.confPswLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.confPswLabel.AutoSize = true;
            this.confPswLabel.Location = new System.Drawing.Point(39, 289);
            this.confPswLabel.Name = "confPswLabel";
            this.confPswLabel.Size = new System.Drawing.Size(120, 13);
            this.confPswLabel.TabIndex = 6;
            this.confPswLabel.Text = "Reinserisci la password:";
            // 
            // confPswTextBox
            // 
            this.confPswTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.confPswTextBox.Location = new System.Drawing.Point(255, 286);
            this.confPswTextBox.Name = "confPswTextBox";
            this.confPswTextBox.Size = new System.Drawing.Size(109, 20);
            this.confPswTextBox.TabIndex = 7;
            this.confPswTextBox.UseSystemPasswordChar = true;
            // 
            // pswTextBox
            // 
            this.pswTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pswTextBox.Location = new System.Drawing.Point(255, 219);
            this.pswTextBox.Name = "pswTextBox";
            this.pswTextBox.Size = new System.Drawing.Size(109, 20);
            this.pswTextBox.TabIndex = 8;
            this.pswTextBox.UseSystemPasswordChar = true;
            // 
            // nickTextBox
            // 
            this.nickTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.nickTextBox.Location = new System.Drawing.Point(255, 149);
            this.nickTextBox.Name = "nickTextBox";
            this.nickTextBox.Size = new System.Drawing.Size(110, 20);
            this.nickTextBox.TabIndex = 9;
            // 
            // usrTextBox
            // 
            this.usrTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.usrTextBox.Location = new System.Drawing.Point(255, 77);
            this.usrTextBox.Name = "usrTextBox";
            this.usrTextBox.Size = new System.Drawing.Size(110, 20);
            this.usrTextBox.TabIndex = 10;
            // 
            // pswCheckBox
            // 
            this.pswCheckBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pswCheckBox.AutoSize = true;
            this.pswCheckBox.Location = new System.Drawing.Point(466, 221);
            this.pswCheckBox.Name = "pswCheckBox";
            this.pswCheckBox.Size = new System.Drawing.Size(106, 17);
            this.pswCheckBox.TabIndex = 11;
            this.pswCheckBox.Text = "Mostra password";
            this.pswCheckBox.UseVisualStyleBackColor = true;
            this.pswCheckBox.CheckedChanged += new System.EventHandler(this.ShowPassword);
            // 
            // confPswCheckBox
            // 
            this.confPswCheckBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.confPswCheckBox.AutoSize = true;
            this.confPswCheckBox.Location = new System.Drawing.Point(465, 287);
            this.confPswCheckBox.Name = "confPswCheckBox";
            this.confPswCheckBox.Size = new System.Drawing.Size(109, 18);
            this.confPswCheckBox.TabIndex = 12;
            this.confPswCheckBox.Text = "Mostra password";
            this.confPswCheckBox.UseCompatibleTextRendering = true;
            this.confPswCheckBox.UseVisualStyleBackColor = true;
            this.confPswCheckBox.CheckedChanged += new System.EventHandler(this.ShowConfPassword);
            // 
            // Register
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(589, 382);
            this.Controls.Add(this.tableLayoutPanel);
            this.Name = "Register";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Register";
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.Button regBtn;
        private System.Windows.Forms.Button escBtn;
        private System.Windows.Forms.Label regLabel;
        private System.Windows.Forms.Label usrLabel;
        private System.Windows.Forms.Label nickLabel;
        private System.Windows.Forms.Label pswLabel;
        private System.Windows.Forms.Label confPswLabel;
        private System.Windows.Forms.TextBox confPswTextBox;
        private System.Windows.Forms.TextBox pswTextBox;
        private System.Windows.Forms.TextBox nickTextBox;
        private System.Windows.Forms.TextBox usrTextBox;
        private System.Windows.Forms.CheckBox pswCheckBox;
        private System.Windows.Forms.CheckBox confPswCheckBox;
    }
}