﻿using System;
using System.Windows.Forms;

namespace OOP18
{
    class Launcher
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(true);
            Application.Run(new Login());
        }
    }
}
