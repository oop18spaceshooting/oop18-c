﻿using System;
using System.Collections.Generic;

namespace OOP18
{
    public class Account : IAccount
    {
        private readonly string username;
        private string password;
        private string nickname;
        private int bestScore;

        public string Username => username;
        public string Password { get => password; set => password = value; }
        public string Nickname { get => nickname; set => nickname = value; }
        public int BestScore { get => bestScore; set => bestScore = value; }

        private Account(string username, string password, string nickname, int bestscore)
        {
            this.username = username;
            this.password = password;
            this.nickname = nickname;
            this.bestScore = bestscore;
        }

        public class Builder
        {
            private readonly string username;
            private readonly string password;
            private string nickname;
            private int score;

            public Builder(string username, string password)
            {
                this.username = username ?? throw new ArgumentNullException();
                this.password = password ?? throw new ArgumentNullException();
            }

            public Builder WithNickname(string nickName)
            {
                this.nickname = nickName ?? throw new ArgumentNullException();
                return this;
            }

            public Builder BestScore(int bestScore)
            {
                if (bestScore < 0)
                {
                    throw new ArgumentException("Illegal Argument Exception");
                }
                this.score = bestScore;
                return this;
            }

            public Account Build() => string.IsNullOrEmpty(this.nickname)
                    ? new Account(this.username, this.password, this.username, this.score)
                    : new Account(this.username, this.password, this.nickname, this.score);
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            return obj is Account account &&
                   username == account.username;
        }

        public override int GetHashCode() => 799926177 + EqualityComparer<string>.Default.GetHashCode(this.username);
    }
}
