﻿namespace OOP18
{
    public interface IAccountManager
    {
        void Register(IAccount account);

        bool IsPresent(IAccount account);

        bool CheckPassword(IAccount account);
    }
}
