namespace powerUp{
 
public interface Entity {

    bool intersects(Entity entity);
   bool isAlive();
    void takeDamage(int damage);
    void destroy();
}
}