using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace powerUp {
    [TestClass]
    public class Testclass{

        [TestMethod]
        public void TestMethod1()
        {
                
            List<EnemyShip> enemyList = new List<EnemyShip>();
            EnemyShip e = new EnemyShipImpl(1,5);
            EnemyShip e1 = new EnemyShipImpl(2,4);
            EnemyShip e2 = new EnemyShipImpl(3,3);
            EnemyShip e3 = new EnemyShipImpl(4,2);
            EnemyShip e4 = new EnemyShipImpl(5,1);
            enemyList.Add(e);
            enemyList.Add(e1);
            enemyList.Add(e2);
            enemyList.Add(e3);
            enemyList.Add(e4);
            FreezePowerUp p= new FreezePowerUp(enemyList);
            NukePowerUp p1= new NukePowerUp(enemyList);
            p.run();
            foreach (EnemyShip enemy in enemyList)
            {
                Console.WriteLine(enemy.isFrozen());
            }
            p1.run();
            foreach (EnemyShip enemy in enemyList)
            {
                Console.WriteLine(enemy.GetType());
            }

        }

    }

}