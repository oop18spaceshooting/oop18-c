using System;
namespace powerUp{
    
public class EnemyShipImpl : EnemyShip {
   
    private  int DEFAULT_SCREEN_X_SIZE = 1920;
    private  int DELAY = 5;
    private  double POSITION_PROPORTION_VALUE = 5;
    private  double DIMENSION_PROPORTION = 0.04;
    private  int STARTING_LIVES = 1;
    private   int STARTING_HEALTH = 1000;
    private   int SCORE_POINTS = 900;
    private  double speed = 0 ;
    private int framesFromShoot;
    private int level;
    private int framesToShoot;
    private bool shootingAvailable;
    private bool frozen;

        public double Speed { get => speed; set => speed = value; }

            public EnemyShipImpl( int level,  int timeToShoot) {
        
        this.level = level;
        this.frozen = false;

    }

    
    public int getLevel() {
        return this.level;
    }
    
    public bool canShoot() {
        return this.shootingAvailable;
    }
    public double getSpeed() {
        return this.Speed;
    }
    
    public int getScorePoints() {
        return SCORE_POINTS;
    }

    public  void setFreeze( bool value) {
        this.frozen = value;
    }

    
    public bool isFrozen() {
        return this.frozen;
    }
     public void destroy(){
     }

     public void nullWarning(bool x, int y){
        this.framesFromShoot = y;
        this.framesToShoot = y;
        this.shootingAvailable = x;
        Console.WriteLine(this.DELAY);
        Console.WriteLine(this.STARTING_HEALTH);
        Console.WriteLine(this.framesFromShoot);
        Console.WriteLine(this.framesToShoot);
        Console.WriteLine(this.POSITION_PROPORTION_VALUE);
        Console.WriteLine(this.STARTING_LIVES);
        Console.WriteLine(this.DIMENSION_PROPORTION);
        Console.WriteLine(this.DEFAULT_SCREEN_X_SIZE);
        Console.WriteLine(this.framesFromShoot);
     }
}
}