using System;
using System.Collections.Generic;

 namespace powerUp{
public class NukePowerUp : PowerUp {

    private  List<EnemyShip> enemies;

    
    public NukePowerUp( List<EnemyShip> enemies) {
        this.enemies = enemies;
    }

    public void run() {
        enemies.Clear();
    }
    public string toString() {
        return "Nuke PowerUp";
    }

}



}
