namespace powerUp{
  
public interface Ship {
    bool intersects(Entity entity);

    bool isAlive();
  void takeDamage(int damage);
    void destroy();
     
}

}