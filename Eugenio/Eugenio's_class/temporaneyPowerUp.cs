namespace powerUp{

 interface TemporaryPowerUp : PowerUp {

    void stop();
    int getDuration();
}


}