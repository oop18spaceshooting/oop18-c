namespace powerUp{
public interface EnemyShip {

    double getSpeed();

    int getLevel();

    bool canShoot();
    int getScorePoints();
    void setFreeze(bool value);
    bool isFrozen();
    void destroy();

}

}