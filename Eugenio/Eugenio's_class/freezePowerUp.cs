using System;
using System.Collections.Generic;
namespace powerUp{

public class FreezePowerUp : TemporaryPowerUp {
    private static  int DURATION = 10;
    private  List<EnemyShip> enemies;

    public FreezePowerUp(List<EnemyShip> enemies) {
        this.enemies = enemies;
    }
    public void run() {
        foreach (EnemyShip enemy in enemies) {
            enemy.setFreeze(true);
        }
    }
    public void stop() {
        this.enemies.Clear();
    }
    public int getDuration() {
        return DURATION;
    }

    
    public string toString() {
        return "Freeze PowerUp";
    }

}
}