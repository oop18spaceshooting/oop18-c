﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SpaceShootingMenu
{
    /// <summary>
    /// Logica di interazione per MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            ImageBrush image = new ImageBrush();
            image.ImageSource =
                new BitmapImage(new Uri("C://Users//Sergiu Popescu//Documents//ESAMI DA DARE//TERZO_ANNO//OOP//SpaceShootingMenu//mainMenu.jpg", UriKind.Absolute));
            this.Background = image;
        }

        private void button1_click(object sender, RoutedEventArgs e)
        {
            Window1 choseDifficulty = new Window1();
            choseDifficulty.Show();
            this.Close();
        }

        private void exit_button_menu(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
