﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SpaceShootingMenu
{
    /// <summary>
    /// Logica di interazione per Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        public Window1()
        {
            InitializeComponent();
            ImageBrush image = new ImageBrush();
            image.ImageSource =
                new BitmapImage(new Uri("C://Users//Sergiu Popescu//Documents//ESAMI DA DARE//TERZO_ANNO//OOP//SpaceShootingMenu//planet.jpg", UriKind.Absolute));
            this.Background = image;
        }

        private void exit_button(object sender, RoutedEventArgs e)
        {
            MainWindow menu = new MainWindow();
            menu.Show();
            this.Close();
        }
    }
}
