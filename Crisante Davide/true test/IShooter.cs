﻿using System.Drawing;

namespace true_test
{
    interface IShooter
    {
     /**
     * <summary>method to shoot.</summary>
     * <returns>the position in which the Bullet appears.</returns>
     */
        PointF Shoot();
    }
}
