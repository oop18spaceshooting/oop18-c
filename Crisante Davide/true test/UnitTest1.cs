﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Drawing;
using System.Collections.Generic;


namespace true_test
{
    [TestClass]
    public class UnitTest1
    {
        private GameController gc = new GameController();

        [TestMethod]
        public void EnemiesMovingTest()
        {
            EnemyController enemy = new EnemyController(gc, new CharacterController(), new SizeF(1000, 1000));
            ((IEnemyShip)(enemy.GetEntity())).Position = new PointF(0, 0);
            gc.AddEnemy(enemy);
            for (int x = 0; x < 10; x++)
            {
                PointF prevPosition = gc.GetEnemyControllerAt(0).GetEntity().Boundary.Location;

                gc.UpdateEnemies();
                Assert.IsTrue(gc.GetEnemyControllerAt(0).GetEntity().Boundary.Location.X > prevPosition.X &&
                    gc.GetEnemyControllerAt(0).GetEntity().Boundary.Location.Y > prevPosition.Y);
            }

            gc.RemoveEnemies();
            ((IEnemyShip)(enemy.GetEntity())).Position = new PointF(1000, 0);
            gc.AddEnemy(enemy);
            for (int x = 0; x < 10; x++)
            {
                PointF prevPosition = gc.GetEnemyControllerAt(0).GetEntity().Boundary.Location;

                gc.UpdateEnemies();
                Assert.IsTrue(gc.GetEnemyControllerAt(0).GetEntity().Boundary.Location.X < prevPosition.X &&
                    gc.GetEnemyControllerAt(0).GetEntity().Boundary.Location.Y > prevPosition.Y);
            }

            gc.RemoveEnemies();
            ((IEnemyShip)(enemy.GetEntity())).Position = new PointF(0, 1000);
            gc.AddEnemy(enemy);
            for (int x = 0; x < 10; x++)
            {
                PointF prevPosition = gc.GetEnemyControllerAt(0).GetEntity().Boundary.Location;

                gc.UpdateEnemies();
                Assert.IsTrue(gc.GetEnemyControllerAt(0).GetEntity().Boundary.Location.X > prevPosition.X &&
                    gc.GetEnemyControllerAt(0).GetEntity().Boundary.Location.Y < prevPosition.Y);
            }

            gc.RemoveEnemies();
            ((IEnemyShip)(enemy.GetEntity())).Position = new PointF(1000, 1000);
            gc.AddEnemy(enemy);
            for (int x = 0; x < 10; x++)
            {
                PointF prevPosition = gc.GetEnemyControllerAt(0).GetEntity().Boundary.Location;

                gc.UpdateEnemies();
                Assert.IsTrue(gc.GetEnemyControllerAt(0).GetEntity().Boundary.Location.X < prevPosition.X &&
                    gc.GetEnemyControllerAt(0).GetEntity().Boundary.Location.Y < prevPosition.Y);
            }

            ((IEnemyShip)(enemy.GetEntity())).Position = new PointF(0, 0);
            ((IEnemyShip)(enemy.GetEntity())).Freeze = true;
            for(int x = 0; x < 10; x++)
            {
                Assert.IsTrue(enemy.GetEntity().Boundary.Location.X == 0 && enemy.GetEntity().Boundary.Location.Y == 0);
            }
        }

        [TestMethod]
        public void EnemiesDamageTest()
        {
            EnemyController enemy = new EnemyController(gc, new CharacterController(), new SizeF(1000, 1000));

            Assert.IsTrue(((AbstractShip)(enemy.GetEntity())).GetMaxHealt() == ((AbstractShip)(enemy.GetEntity())).GetHealth());
            ((IEnemyShip)(enemy.GetEntity())).TakeDamage(10);
            Assert.IsTrue(((AbstractShip)(enemy.GetEntity())).GetMaxHealt() > ((AbstractShip)(enemy.GetEntity())).GetHealth());
            ((IEnemyShip)(enemy.GetEntity())).TakeDamage(((AbstractShip)(enemy.GetEntity())).GetMaxHealt());
            Assert.IsTrue(((AbstractShip)(enemy.GetEntity())).GetHealth() < 0);
            Assert.IsFalse(((IEnemyShip)(enemy.GetEntity())).IsAlive());
        }

        [TestMethod]
        public void EnemiesShootinTest()
        {
            gc.AddEnemy(new EnemyController(gc, new CharacterController(), new SizeF(1000, 1000)));
            EnemyController enemy = gc.GetEnemyControllerAt(0);

            Assert.IsTrue(gc.GetBulletControllers().Count == 0);
            gc.ForcedShoot();
            Assert.IsTrue(gc.GetBulletControllers().Count == 1);

            gc.ForcedShoot();
            gc.ForcedShoot();
            gc.ForcedShoot();
            gc.ForcedShoot();
            Assert.IsTrue(gc.GetBulletControllers().Count == 5);
        }

        [TestMethod]
        public void EnemyIntersectsTest()
        {
            gc.AddEnemy(new EnemyController(gc, new CharacterController(), new SizeF(1000, 1000)));

            Assert.IsTrue(gc.GetEnemyControllers().Count == 1);

            EnemyShipImpl enemy = (EnemyShipImpl)gc.GetEnemyControllerAt(0).GetEntity();
            Assert.IsTrue(enemy.Intersects(enemy));

            gc.UpdateEnemies();

            Assert.IsTrue(gc.GetEnemyControllers().Count == 0);
        }

        [TestMethod]
        public void BulletMovingTest()
        {
            gc.RemoveBullets();
            Assert.IsTrue(gc.GetBulletControllers().Count == 0);
            BulletController bullet = new BulletController(gc, new PointF(0, 0), new CharacterController().CentralPosition, new SizeF(1000, 1000));

            gc.AddBullet(bullet);
            for (int x = 0; x < 10; x++)
            {
                PointF prevPosition = gc.GetBulletControllerAt(0).GetEntity().Boundary.Location;

                gc.UpdateBullets();
                Assert.IsTrue(gc.GetBulletControllerAt(0).GetEntity().Boundary.Location.X > prevPosition.X &&
                    gc.GetBulletControllerAt(0).GetEntity().Boundary.Location.Y > prevPosition.Y);
            }

            gc.RemoveBullets();
            bullet = new BulletController(gc, new PointF(1000, 0), new CharacterController().CentralPosition, new SizeF(1000, 1000)); ;
            gc.AddBullet(bullet);
            for (int x = 0; x < 10; x++)
            {
                PointF prevPosition = gc.GetBulletControllerAt(0).GetEntity().Boundary.Location;

                gc.UpdateBullets();
                Assert.IsTrue(gc.GetBulletControllerAt(0).GetEntity().Boundary.Location.X < prevPosition.X &&
                    gc.GetBulletControllerAt(0).GetEntity().Boundary.Location.Y > prevPosition.Y);
            }

            gc.RemoveBullets();
            bullet = new BulletController(gc, new PointF(0, 1000), new CharacterController().CentralPosition, new SizeF(1000, 1000)); ;
            gc.AddBullet(bullet);
            for (int x = 0; x < 10; x++)
            {
                PointF prevPosition = gc.GetBulletControllerAt(0).GetEntity().Boundary.Location;

                gc.UpdateBullets();
                Assert.IsTrue(gc.GetBulletControllerAt(0).GetEntity().Boundary.Location.X > prevPosition.X &&
                    gc.GetBulletControllerAt(0).GetEntity().Boundary.Location.Y < prevPosition.Y);
            }

            gc.RemoveBullets();
            bullet = new BulletController(gc, new PointF(1000, 1000), new CharacterController().CentralPosition, new SizeF(1000, 1000)); ;
            gc.AddBullet(bullet);
            for (int x = 0; x < 10; x++)
            {
                PointF prevPosition = gc.GetBulletControllerAt(0).GetEntity().Boundary.Location;

                gc.UpdateBullets();
                Assert.IsTrue(gc.GetBulletControllerAt(0).GetEntity().Boundary.Location.X < prevPosition.X &&
                    gc.GetBulletControllerAt(0).GetEntity().Boundary.Location.Y < prevPosition.Y);
            }
        }

        [TestMethod]
        public void BulletMakeDamageTest()
        {
            BulletController bullet = new BulletController(gc, new PointF(0, 0), new CharacterController().CentralPosition, new SizeF(1000, 1000));
            EnemyController enemy = new EnemyController(gc, new CharacterController(), new SizeF(1000, 1000));
            ((IEnemyShip)(enemy.GetEntity())).Position = new PointF(0, 0);

            gc.AddBullet(bullet);
            gc.AddEnemy(enemy);
            Assert.IsTrue(gc.GetBulletControllers().Count == 1 && gc.GetEnemyControllers().Count == 1);
            Assert.IsTrue(((AbstractShip)(gc.GetEnemyControllerAt(0).GetEntity())).GetLife() == 1 && ((AbstractShip)(gc.GetEnemyControllerAt(0).GetEntity())).GetHealth() == 1000);
            Assert.IsTrue(gc.GetEnemyControllerAt(0).GetEntity().Intersects(gc.GetBulletControllerAt(0).GetEntity()));
            //gc.GetBulletControllerAt(0).GetEntity().Intersects(gc.GetEnemyControllerAt(0).GetEntity());

            Assert.IsTrue(((AbstractShip)(gc.GetEnemyControllerAt(0).GetEntity())).GetLife() == 0 && ((AbstractShip)(gc.GetEnemyControllerAt(0).GetEntity())).GetHealth() == 0 );
            gc.UpdateBullets();
            gc.UpdateEnemies();
            Assert.IsTrue(gc.GetBulletControllers().Count == 0 && gc.GetEnemyControllers().Count == 0);
        }

    }
}
