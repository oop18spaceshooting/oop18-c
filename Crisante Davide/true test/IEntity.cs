﻿using System.Drawing;

namespace true_test
{
    interface IEntity
    {

        RectangleF Boundary { get; }

        /**
        * <summary>Method that checks the intersection between this entity and the entity received as parameter.</summary>
        * 
        * <param name="entity"> entity the entity which should be checked the intersection with.</param>
        * 
        * <returns> true if the entity is intersected, false otherwise.</returns>
        */
        bool Intersects(IEntity entity);

        /**
        * <summary>Method that says if this entity is still alive or not.</summary>
        * 
        * <returns> true if the entity is still alive, false otherwise</returns>
        */
        bool IsAlive();

        /**
        * <summary>Method that damages this entity by damage received as parameter.</summary>
        * 
        * <param name="damage">damage the value of the damage taken.</param>
        */
        void TakeDamage(int damage);

        /**
        * <summary>Method that sets the life of this entity to 0.</summary>
        */
        void Destroy();
    }
}