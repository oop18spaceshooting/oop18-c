﻿namespace true_test
{
    interface IEntityController
    {
        /**
        *  <summary>Method that updates the position of the entity.</summary>
        */
        void Update();

        /**
         *  <summary>Method that orders to draw the entity to the view.</summary>
         */
        void Draw();

        /**
         *  <summary>Method that gives the entity of the controller.</summary>
         *
         *  <returns> the entity of the controller</returns>
         */
        IEntity GetEntity();

        /**
         * <summary>Method that executes the destroying animation.</summary>
         */
        void Destroy();
    }
}
