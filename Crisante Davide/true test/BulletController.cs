﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace true_test
{
    class BulletController : IEntityController
    {
        private readonly Image image;
        private readonly GameController gameController;
        private readonly IBullet bullet;

        /**
         * <summary>Build a BulletController and his easy-level Bullet.</summary>
         * <param name="gameController">gameController the gameController.</param>
         * <param name="level">level the level of the Bullet to create.</param>
         * <param name="src">src the starting point of the Bullet to create.</param>
         * <param name="target"> target the target point of the Bullet.</param>
         * <param name="fieldSize">fieldSize the field width and height.</param>
         */
        public BulletController(GameController gameController, int level, PointF src, PointF target, SizeF fieldSize)
        {
            this.image = null;   //utilities.ImageUtils.getBulletImage(level);
            this.bullet = new BulletImpl(level, src, target, fieldSize);
            this.gameController = gameController;
        }

        /**
         * <summary>Build a BulletController and his easy-level Bullet.</summary>
         * <param name="gameController">gameController the gameController.</param>
         * <param name="src">src the starting point of the Bullet to create.</param>
         * <param name="target"> target the target point of the Bullet.</param>
         * <param name="fieldSize">fieldSize the field width and height.</param>
         */
        public BulletController(GameController gameController, PointF src, PointF target, SizeF fieldSize) : this(gameController, 1, src, target, fieldSize)
        {
        }

        public int Damage
        {
            get
            {
                return this.bullet.Damage;
            }
        }

        public void Destroy()
        {
            this.gameController.RemoveEnemyBullet(this);
        }

        public void Draw()
        {
            this.gameController.DrawEntity(this.image, this.bullet.Angle, this.bullet.Boundary);
        }

        public IEntity GetEntity()
        {
            return this.bullet;
        }

        public void Update()
        {
            this.bullet.Update();
        }
    }
}
