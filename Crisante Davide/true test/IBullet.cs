﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace true_test
{
    interface IBullet : IEntity
    {

        /**
         * <summary>Update the Bullet position.</summary>
         */
        void Update();

        double Angle { get; }

        int Damage { get; }

    }
}
