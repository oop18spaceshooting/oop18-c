﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace true_test
{
    class GameController
    {
        private LinkedList<BulletController> bullets = new LinkedList<BulletController>();
        private LinkedList<EnemyController> enemies = new LinkedList<EnemyController>();

        public void UpdateBullets()
        {
            foreach (BulletController bullet in this.bullets.ToList())
            {
                if(!bullet.GetEntity().IsAlive())
                {
                    bullet.Destroy();
                }
                else
                {
                    bullet.Update();
                }
            }
        }

        public void UpdateEnemies()
        {
            foreach (EnemyController enemy in this.enemies.ToList())
            {
                enemy.Update();
                if (!enemy.GetEntity().IsAlive())
                {
                    enemy.Destroy();
                }
            }
        }
        
        public void AddBullet(BulletController bullet)
        {
            this.bullets.AddFirst(bullet);
        }

        public void AddEnemy(EnemyController enemy)
        {
            this.enemies.AddFirst(enemy);
        }

        public BulletController ForcedShoot()
        {
            foreach(EnemyController enemy in this.enemies.ToList())
            {
                this.bullets.AddLast(enemy.Shoot());
            }
            return this.bullets.ElementAt(this.bullets.Count - 1);
        }

        public void RemoveEnemyBullet(BulletController bullet)
        {
            this.bullets.Remove(bullet);
        }

        public void RemoveEnemy(EnemyController enemy)
        {
            this.enemies.Remove(enemy);
        }

        public void DrawEntity(Image image, double angle, RectangleF boundary)
        {
            Console.WriteLine("Entity printed successfully.");
        }

        public LinkedList<BulletController> GetBulletControllers()
        {
            return this.bullets;
        }

        public void RemoveEnemies()
        {
            enemies.Clear();
        }

        public void RemoveBullets()
        {
            bullets.Clear();
        }

        public BulletController GetBulletControllerAt(int index)
        {
            return this.bullets.ElementAt(index);
        }

        public EnemyController GetEnemyControllerAt(int index)
        {
            return this.enemies.ElementAt(index);
        }

        public LinkedList<EnemyController> GetEnemyControllers()
        {
            return this.enemies;
        }
    }
}
