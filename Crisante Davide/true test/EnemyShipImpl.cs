﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace true_test
{
    class EnemyShipImpl : AbstractShip, IEnemyShip
    {
        private const int DEFAULT_SCREEN_X_SIZE = 1920;
        private const double POSITION_PROPORTION_VALUE = 5;
        private const double DIMENSION_PROPORTION = 0.04;
        private const int STARTING_LIVES = 1;
        private const int STARTING_HEALTH = 1000;
        private const int SCORE_POINTS = 900;
        private PointF position;
        private SizeF shipDimension;
        private readonly double speed;
        private int framesFromShoot;
        private readonly int level;
        private readonly int framesToShoot;
        private bool shootingAvailable;
        private bool frozen;

     /**
     * <summary>Build a new EnemyShip.</summary>
     * <param name="level"> level is the level (fastness, power of bullets)
     * <param name="timeToShoot"> timeToShoot frames passing from one bullet-shot to the next one</param>
     * <param name="fieldSize"> fieldSize the field width and height.</param>
     * <param name="characterPosition"> characterPosition the position of the character when the ship is created.</param>
     * <param name="myPosition"> myPosition the enemyship starting position.</param>
     */
        public EnemyShipImpl(int level, int timeToShoot, SizeF fieldSize, PointF characterPosition, PointF myPosition) : base(STARTING_LIVES, STARTING_HEALTH)
        {
            Random random = new Random();
            this.level = level;
            double speedUnit = fieldSize.Width / DEFAULT_SCREEN_X_SIZE;
            this.speed = speedUnit;
            this.framesToShoot = timeToShoot;
            this.Freeze = false;
            this.position = myPosition;
            double xSize = fieldSize.Width * DIMENSION_PROPORTION;
            this.shipDimension = new SizeF((float)xSize, (float)xSize);
        }

        /**
        * <summary>Build a new EnemyShip.</summary>
        * <param name="level"> level is the level (fastness, power of bullets)
        * <param name="timeToShoot"> timeToShoot frames passing from one bullet-shot to the next one</param>
        * <param name="fieldSize"> fieldSize the field width and height.</param>
        * <param name="characterPosition"> characterPosition the position of the character when the ship is created.</param>
        */
        public EnemyShipImpl(int level, int timeToShoot, SizeF fieldSize, PointF characterPosition) : base(STARTING_LIVES, STARTING_HEALTH)
        {
            Random random = new Random();
            this.level = level;
            double speedUnit = fieldSize.Width / 1920;
            this.speed = speedUnit;
            this.framesToShoot = timeToShoot;
            this.Freeze = false;
            double provX = random.NextDouble() * fieldSize.Width;
            double provY = random.NextDouble() * fieldSize.Height;
            if (Math.Abs(provX - characterPosition.X) < fieldSize.Width / POSITION_PROPORTION_VALUE)
            {
                provX = characterPosition.X + fieldSize.Width / POSITION_PROPORTION_VALUE;
                if (provX < 0 || provX > fieldSize.Width)
                {
                    provX = -provX;
                }
            }
            if (Math.Abs(provY - characterPosition.Y) < fieldSize.Height / POSITION_PROPORTION_VALUE)
            {
                provY = characterPosition.Y + fieldSize.Height / POSITION_PROPORTION_VALUE;
                if (provY < 0 || provY > fieldSize.Height)
                {
                    provY = -provY;
                }
            }
            this.Position = new PointF((float)provX, (float)provY);
            double xSize = fieldSize.Width * DIMENSION_PROPORTION;
            this.shipDimension = new SizeF((float)xSize, (float)xSize);
        }

        /**
        * <summary>Build a new EnemyShip.</summary>
        * <param name="level"> level is the level (fastness, power of bullets)
        * <param name="fieldSize"> fieldSize the field width and height.</param>
        * <param name="characterPosition"> characterPosition the position of the character when the ship is created.</param>
        */
        public EnemyShipImpl(int level, SizeF fieldSize, PointF characterPosition) : this(level, (int)((new Random().NextDouble() * 600) + 200), fieldSize, characterPosition)
        {
        }

        /**
        * <summary>Build a new EnemyShip.</summary>
        * <param name="fieldSize"> fieldSize the field width and height.</param>
        * <param name="characterPosition"> characterPosition the position of the character when the ship is created.</param>
        */
        public EnemyShipImpl(SizeF fieldSize, PointF characterPosition) : this(1, fieldSize, characterPosition)
        {
        }

        public bool CanShoot()
        {
            return this.shootingAvailable;
        }

        public int Level
        {
            get
            {
                return this.level;
            }
        }

        public int ScorePoints
        {
            get
            {
                return SCORE_POINTS;
            }
        }

        public double Speed
        {
            get
            {
                return this.speed;
            }
        }

        public bool IsFrozen()
        {
            return this.frozen;
        }

        public bool Freeze
        {
            get
            {
                return this.frozen;
            }
            set
            {
                this.frozen = value;
            }
        }

        public override PointF Position
        {
            get
            {
                return this.position;
            }
            set
            {
                this.position = value;
            }
        }

        public void Update(PointF addPosition)
        {
            if (!frozen)
            {
                Position = new PointF(this.position.X + addPosition.X, this.position.Y + addPosition.Y);
                this.framesFromShoot++;
                if(framesFromShoot >= framesToShoot)
                {
                    this.shootingAvailable = true;
                }
            }
        }

        public  override SizeF Dimension
        {
            get
            {
                return this.shipDimension;
            }
            set
            {
                this.shipDimension = value;
            }
        }

        public override bool Intersects(IEntity entity)
        {
            bool isIntersected = this.Boundary.IntersectsWith(entity.Boundary);
            if (isIntersected)
            {
                base.Destroy();
                entity.Destroy();
            }
            return isIntersected;
        }

        public override PointF Shoot()
        {
            this.framesFromShoot = 0;
            this.shootingAvailable = false;
            return new PointF(this.position.X + (this.shipDimension.Width / 2), this.position.Y * (this.shipDimension.Height / 2));
        }
    }
}
