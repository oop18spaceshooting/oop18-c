﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace true_test
{
    class CharacterController
    {
        private SizeF Dimensions = new SizeF(100, 100);
        public PointF CentralPosition
        {
            get
            {
                return new PointF(500, 500);
            }
        }

        public RectangleF Boundary
        {
            get
            {
                return new RectangleF(CentralPosition, Dimensions);
            }
        }
    }
}
