﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;

namespace true_test
{
    class BulletImpl : IBullet
    {
        private const int DEFAULT_SCREEN_X_SIZE = 1920;
        private const int DAMAGE_UNIT = 500;
        private const double X_DIMENSION_PROPORTION = 0.003;
        private const double POPORTION_BETWEEN_SIZES = 7;
        private const double SPEED_UNIT = 3.5;
        private readonly double speed;
        private readonly int damage;
        private PointF position;
        private readonly SizeF shipDimension;
        private PointF target;
        private readonly double movX;
        private readonly double movY;
        private readonly double angle;
        private readonly SizeF fieldSize;

    /**
     * <summary>Build a new Bullet.</summary>
     * <param name="level"> level defines the speed.</param>
     * <param name="src"> the starting position of the Bullet.</param>
     * <param name="target"> the target position.</param>
     * <param name="fieldSize">the field width and height.</param>
     */
    public BulletImpl(int level, PointF src, PointF target, SizeF fieldSize)
        {
            this.damage = 500;//GameUtils.transform(DAMAGE_UNIT, level); in the Java version
            this.speed = (fieldSize.Width / DEFAULT_SCREEN_X_SIZE) * SPEED_UNIT;
            this.fieldSize = fieldSize;
            this.position = src;
            this.angle = Math.Atan2(src.Y - target.Y, src.X - target.X);
            this.movY = -speed * Math.Sin(this.angle);
            this.movX = -speed * Math.Cos(this.angle);
            this.target = new PointF(src.X + (float)movX, src.Y + (float)movY);
            float xSize = fieldSize.Width * (float)X_DIMENSION_PROPORTION;
            this.shipDimension = new SizeF(xSize, xSize * (float)POPORTION_BETWEEN_SIZES);
        }
        public void Destroy()
        {
            this.position = new PointF(-1, -1);
        }

        public double Angle
        {
            get
            {
                return this.angle;
            }
        }

        public RectangleF Boundary
        {
            get
            {
                return new RectangleF(this.position.X, this.position.Y, this.shipDimension.Width, this.shipDimension.Height);
            }
        }

        public int Damage
        {
            get
            {
                return this.damage;
            }
        }

        public bool Intersects(IEntity entity)
        {
            return entity.Boundary.IntersectsWith(this.Boundary);
        }

        public bool IsAlive()
        {
            return !(position.X > this.fieldSize.Width || position.Y > this.fieldSize.Height
                || position.X < 0 || position.Y < 0);
        }

        public void TakeDamage(int damage)
        {
        }

        public void Update()
        {
            position = new PointF(position.X + (float)(movX * speed), position.Y + (float)(movY * speed));
            target = new PointF(target.X + (float)(movX * speed), target.Y + (float)(movY * speed));
        }
    }
}
