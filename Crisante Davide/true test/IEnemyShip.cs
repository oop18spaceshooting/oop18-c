﻿using System.Drawing;

namespace true_test
{
    interface IEnemyShip : IShip
    {

        double Speed { get; }

        int Level { get; }

        /**
         * <summary>Update the Enemy position.</summary>
         * <param name="addPosition"> addPosition the position to add at the current position.</param>
         */
        void Update(PointF addPosition);

        /**
         * <summary>Return true if the EnemyShip can shoot.</summary>
         * <returns>true if the EnemyShip can shoot.</returns>
         */
        bool CanShoot();

        int ScorePoints { get; }

        bool Freeze { get; set; }

        PointF Position { get; set; }
    }
}