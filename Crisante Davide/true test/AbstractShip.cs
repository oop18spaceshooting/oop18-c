﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace true_test
{
    abstract class AbstractShip : IShip
    {
        private int life;
        private int health;
        public readonly int MaxHealth;

        /**
        * <summary>The constructor for the abstract Ship.</summary>
        * <param name="lives"> lives the number of lives</param>
        * <param name="startingHealth">the number of initial health points</param>
        */
        public AbstractShip(int lives, int startingHealth)
        {
            this.MaxHealth = startingHealth;
            this.life = lives;
            this.health = startingHealth;
        }

        public void Destroy()
        {
            this.TakeDamage(this.health);
        }

        public RectangleF Boundary
        {
            get
            {
                return new RectangleF(this.Position.X, this.Position.Y, Dimension.Width, Dimension.Height);
            }
        }

        public abstract bool Intersects(IEntity entity);

        public bool IsAlive()
        {
            return (this.health > 0 && this.life >= 1);
        }

        public abstract PointF Shoot();

        public void TakeDamage(int damage)
        {
            this.health -= damage;
            if(this.health <= 0)
            {
                this.life--;
                if(this.life > 1)
                {
                    damage = Math.Abs(health);
                    health = this.MaxHealth;
                    TakeDamage(damage);
                }
            }
        }

        public int GetMaxHealt()
        {
            return this.MaxHealth;
        }

        public int GetLife()
        {
            return this.life;
        }

        public int GetHealth()
        {
            return this.health;
        }

        public abstract PointF Position { get; set; }

        public abstract SizeF Dimension { get; set; }
    }
}
