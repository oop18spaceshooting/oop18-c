﻿using System;
using System.Drawing;

namespace true_test
{
    class EnemyController : IEntityController
    {
        private readonly Image image;
        private readonly CharacterController characterController;
        private readonly GameController gameController;
        private readonly IEnemyShip enemy;
        private SizeF fieldSize;
        private double rad;


        /**
         * <summary>Build a new EnemyController and his EnemyShip.</summary>
         * <param name="gameController"> gameController the fieldView</param>
         * <param name="level"> level the level of the new created Enemy.</param>
         * <param name="characterController"> characterController the entity representing the CharacterShip.</param>
         * <param name="fieldSize"> fieldSize the field width and height.</param>
         */
        public EnemyController(GameController gameController, int level, CharacterController characterController, SizeF fieldSize)
        {
            this.fieldSize = fieldSize;
            this.image = null;      //utilities.ImageUtils.getEnemyShipImage(level);
            this.enemy = new EnemyShipImpl(level, fieldSize, characterController.CentralPosition);
            this.gameController = gameController;
            this.characterController = characterController;
        }

        /**
         * <summary>Build a new EnemyController and his EnemyShip.</summary>
         * <param name="gameController"> gameController the fieldView</param>
         * <param name="characterController"> characterController the entity representing the CharacterShip.</param>
         * <param name="fieldSize"> fieldSize the field width and height.</param>
         */
        public EnemyController(GameController gameController, CharacterController characterController, SizeF fieldSize) : this(gameController, 1, characterController, fieldSize)
        {
        }
        /**
        * <summary>Creates and returns a new BulletController</summary>
        * <returns> the new Bullet.</returns>
        */
        public BulletController Shoot()
        {
            return new BulletController(this.gameController, this.enemy.Level, this.enemy.Shoot(),
                    this.characterController.Boundary.Location, this.fieldSize);
        }

        /**
         * <returns> true if the enemy can shoot according to his shooting rateo.</returns>
         */
        public bool CanShoot()
        {
            return this.enemy.CanShoot();
        }

        public void Update()
        {
            double movY, movX;
            PointF centralPosition = new PointF((enemy.Boundary.Left + enemy.Boundary.Right) / 2,
                    (enemy.Boundary.Top + enemy.Boundary.Bottom) / 2);
            this.rad = Math.Atan2(centralPosition.Y - this.characterController.CentralPosition.Y,
                    centralPosition.X - this.characterController.CentralPosition.X);

            movY = -this.enemy.Speed * Math.Sin(this.rad);
            movX = -this.enemy.Speed * Math.Cos(this.rad);
            enemy.Update(new PointF((float)movX, (float)movY));
        }

        public void Draw()
        {
            this.gameController.DrawEntity(this.image, this.rad, this.enemy.Boundary);
        }

        public IEntity GetEntity()
        {
            return this.enemy;
        }

        public void Destroy()
        {
            this.gameController.RemoveEnemy(this);
        }
    }
}
