﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SpaceShootingCharacter
{
    class CharacterShip : ICharacterShip
    {
        private static readonly int DEFAULT_DIMENSION = 1920;
        private static readonly double DIMENSION_PROPORTION = 0.04;
        private static readonly double SPEED_UNIT = 0.4;
        private readonly double speed;
        private readonly Size dimention;
        private Point position;

        public CharacterShip(Point InitialPosition, Rectangle resolution)
        {
            this.speed = (DEFAULT_DIMENSION / resolution.Width) * SPEED_UNIT;
            double sideSize = resolution.Width * DIMENSION_PROPORTION;
            this.dimention = new Size((int) sideSize, (int) sideSize);
            this.position = new Point(InitialPosition.X - this.dimention.Width / 2, InitialPosition.Y - this.dimention.Height / 2);
        }

        public Rectangle Boundary
        {
            get { return new Rectangle(position, dimention); }

        }

        public double Speed
        {
            get { return this.speed; }
        }

        public void Update(int x, int y)
        {
            this.position = new Point(x, y);
        }
    }
}
