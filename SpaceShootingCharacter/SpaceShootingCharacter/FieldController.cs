﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SpaceShootingCharacter
{
    class FieldController
    {
        private static readonly Rectangle RESOLUTION = System.Windows.Forms.Screen.PrimaryScreen.Bounds;
        private readonly CharacterController character;
        private readonly CameraController camera;
        private readonly FieldDraw fieldDraw;

        public FieldController()
        {
            this.camera = new CameraController();
            this.character = new CharacterController(RESOLUTION, this.camera);
            this.fieldDraw = new FieldDraw(this);
        }

        public CharacterController Character
        {
            get { return this.character; }
        }

        public Size Resolution
        {
            get { return RESOLUTION.Size; }
        }

        public FieldDraw FieldDraw
        {
            get { return this.fieldDraw; }
        }

        public CameraController Camera
        {
            get { return this.camera; }
        }
    }
}
