﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceShootingCharacter
{
    class CameraController
    {
        private double positionValueX;
        private double positionValueY;

        public CameraController()
        {
            this.positionValueX = 0;
            this.positionValueY = 0;
        }

        public Point Position
        {
            get { return new Point((int) this.positionValueX, (int) this.positionValueY); }

            set
            {
                this.positionValueX += value.X;
                this.positionValueY += value.Y;
            }
        }
    }
}
