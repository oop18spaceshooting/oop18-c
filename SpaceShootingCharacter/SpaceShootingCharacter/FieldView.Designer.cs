﻿namespace SpaceShootingCharacter
{
    partial class FieldView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.ship = new System.Windows.Forms.PictureBox();
            this.field = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.ship)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.field)).BeginInit();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 15;
            this.timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // ship
            // 
            this.ship.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ship.BackColor = System.Drawing.Color.Transparent;
            this.ship.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ship.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.ship.Image = global::SpaceShootingCharacter.Properties.Resources.ship;
            this.ship.Location = new System.Drawing.Point(363, 191);
            this.ship.Name = "ship";
            this.ship.Size = new System.Drawing.Size(58, 58);
            this.ship.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ship.TabIndex = 0;
            this.ship.TabStop = false;
            // 
            // field
            // 
            this.field.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.field.BackColor = System.Drawing.Color.Transparent;
            this.field.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.field.Image = global::SpaceShootingCharacter.Properties.Resources.background;
            this.field.Location = new System.Drawing.Point(0, 0);
            this.field.Name = "field";
            this.field.Size = new System.Drawing.Size(800, 450);
            this.field.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.field.TabIndex = 1;
            this.field.TabStop = false;
            // 
            // FieldView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.ship);
            this.Controls.Add(this.field);
            this.Name = "FieldView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SpaceShooting";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.ship)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.field)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox ship;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox field;
    }
}