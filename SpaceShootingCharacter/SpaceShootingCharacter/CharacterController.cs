﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceShootingCharacter
{
    class CharacterController
    {
        private static readonly char separator = Path.DirectorySeparatorChar;
        private static readonly string projectDirectory = Directory.GetCurrentDirectory() + separator + ".." + separator + "..";
        private static readonly string EXTENSION = ".png";
        private static readonly Image shipImage = Image.FromFile(projectDirectory + separator + "ship" + EXTENSION);
        private readonly Rectangle fieldDimension;
        private readonly CharacterShip ship;
        private readonly CameraController camera;
        private float angle;
        private long lastUpdate;

        public CharacterController(Rectangle fieldDimension, CameraController camera)
        {
            this.camera = camera;
            this.fieldDimension = fieldDimension;
            this.ship = new CharacterShip(new Point(this.fieldDimension.Width / 2, this.fieldDimension.Height / 2), this.fieldDimension);
            this.lastUpdate = System.DateTime.Now.Ticks;
        }

        private Point GetUpdatedPosition()
        {
            Point mousePosition = System.Windows.Forms.Cursor.Position;
            Point vector = new Point(mousePosition.X - this.fieldDimension.Width / 2, mousePosition.Y - this.fieldDimension.Height / 2);
            long time = System.DateTime.Now.Ticks;
            double timeFromLastUpdate = (time - this.lastUpdate) * 0.0001;
            this.lastUpdate = time;
            this.angle = (float) Math.Atan2(vector.Y, vector.X);
            double transX = timeFromLastUpdate * this.ship.Speed * Math.Cos(this.angle);
            double transY = timeFromLastUpdate * this.ship.Speed * Math.Sin(this.angle);
            double shipUpdateX = this.ship.Boundary.X + (transX);
            double shipUpdateY = this.ship.Boundary.Y + (transY);
            if (shipUpdateX < 0)
            {
                shipUpdateX = 0;
            }
            else if (shipUpdateX > (this.fieldDimension.Width - this.ship.Boundary.Width))
            {
                shipUpdateX = this.fieldDimension.Width - this.ship.Boundary.Width;
            }
            if (shipUpdateY < 0)
            {
                shipUpdateY = 0;
            }
            else if (shipUpdateY > (this.fieldDimension.Height - this.ship.Boundary.Height))
            {
                shipUpdateY = this.fieldDimension.Height - this.ship.Boundary.Height;
            }
            return new Point((int) shipUpdateX, (int) shipUpdateY);
        }

        public void Update()
        {
            Point updatedPosition = this.GetUpdatedPosition();
            this.camera.Position = new Point(this.ship.Boundary.X - updatedPosition.X, this.ship.Boundary.Y - updatedPosition.Y);
            this.ship.Update(updatedPosition.X, updatedPosition.Y);
        }

        public Image Image
        {
            get { return shipImage; }
        }

        public CharacterShip Ship
        {
            get { return this.ship; }
        }

        public float Angle
        {
            get { return this.angle; }
        }
    }
}
