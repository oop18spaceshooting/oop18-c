﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace SpaceShootingCharacter
{
    interface ICharacterShip
    {
        void Update(int x, int y);

        Rectangle Boundary { get; }
    }
}
