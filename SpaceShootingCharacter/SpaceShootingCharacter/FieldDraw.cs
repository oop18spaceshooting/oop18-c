﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SpaceShootingCharacter
{
    class FieldDraw
    {
        private readonly FieldController field;

        public FieldDraw(FieldController field)
        {
            this.field = field;
        }
        
        public void Update()
        {
            this.field.Character.Update();
        }
    }
}
