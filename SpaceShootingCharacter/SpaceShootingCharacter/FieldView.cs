﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SpaceShootingCharacter
{
    public partial class FieldView : Form
    {
        private readonly FieldController fieldController;
        private readonly Image shipImage;

        public FieldView()
        {
            InitializeComponent();
            this.field.Parent = this.FindForm();
            this.ship.Parent = this.field;
            this.timer1.Interval = 15;
            this.timer1.Start();
            this.fieldController = new FieldController();
            this.field.Size = this.fieldController.Resolution;
            this.ship.Size = this.fieldController.Character.Ship.Boundary.Size;
            this.shipImage = this.ship.Image;
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            this.fieldController.FieldDraw.Update();
            this.ship.Location = this.fieldController.Character.Ship.Boundary.Location;
            this.field.Location = this.fieldController.Camera.Position;
            this.ship.Image = RotateImage(this.shipImage, (float) (this.fieldController.Character.Angle * (180 / Math.PI)));
        }

        public Image RotateImage(Image img, float rotationAngle)
        {
            Bitmap bmp = new Bitmap(this.ship.Width, this.ship.Height);
            Graphics gfx = Graphics.FromImage(bmp);
            
            gfx.TranslateTransform((float)bmp.Width / 2, (float)bmp.Height / 2);
            gfx.RotateTransform(rotationAngle + 90);
            gfx.TranslateTransform(-(float)bmp.Width / 2, -(float)bmp.Height / 2);
            gfx.InterpolationMode = InterpolationMode.HighQualityBicubic;
            gfx.DrawImage(img, new Rectangle(0, 0, this.ship.Width, this.ship.Height));
            gfx.Dispose();
            
            return bmp;
        }
    }
}
