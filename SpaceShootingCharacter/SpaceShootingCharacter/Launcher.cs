﻿using System;
using System.Windows.Forms;

namespace SpaceShootingCharacter
{
    class Launcher
    {
        [STAThread]
        static void Main(string[] args)
        {
            FieldView fieldView = new FieldView();
            Application.Run(fieldView);
        }
    }
}
