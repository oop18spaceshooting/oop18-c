﻿using System;
using System.Linq;
using System.Text;

interface IScore
{
    
    void addScore(int value);
    
    int getScorePoints();
  
}
