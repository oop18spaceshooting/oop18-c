﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpaceShooting
{
    interface ILife
    {
        void loseHealth(int health);
        void addHealth(int health);
        void addLife();
        void loseLife();
        int getCurrentHealth();
        int getHealth();
        int getLives();

    }
}
