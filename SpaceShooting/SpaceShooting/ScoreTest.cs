using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.CompilerServices;
using System.Threading;
namespace SpaceShooting
{
    [TestClass]
    public class ScoreTest
    {
        private IScore score;
        private static int VALUE = 100;
        private static int NEGATIVE_VALUE = -100;
        [TestMethod]
        public void testGetter()
        {
            this.score = new ScoreImpl();
            Assert.AreEqual(0, this.score.getScorePoints());
        }
        [TestMethod]
        public void testAddScore()
        {
            this.score = new ScoreImpl();
            this.score.addScore(VALUE);
            Assert.AreEqual(VALUE, this.score.getScorePoints());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestOnlyPositiveNumbers()
        {
            this.score = new ScoreImpl();
            this.score.addScore(NEGATIVE_VALUE);
        }

        [TestMethod]
        public void TestMultipleAddScores()
        {
            this.score = new ScoreImpl();
            for(int i=0; i < VALUE; i++)
            {
                this.score.addScore(i);
            }
            Assert.AreEqual((VALUE * (VALUE - 1)) / 2, this.score.getScorePoints());
        }
    }
}
