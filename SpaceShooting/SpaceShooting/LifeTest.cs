﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Runtime.CompilerServices;
using System.Threading;

namespace SpaceShooting
{
    [TestClass]
    public class LifeTest
    {

        private static int INITIAL_LIVES = 3;
        private static int INITIAL_HEALTH = 1000;
        private static int DAMAGE_AMOUNT = 300;
        private static int RECOVER_AMOUNT = 150;
        [TestMethod]
        public void healthTest()
        {
            ILife life = LifeImpl.createCustomizedLife(INITIAL_LIVES, INITIAL_HEALTH);
            life.loseHealth(DAMAGE_AMOUNT);
            Assert.AreEqual(INITIAL_HEALTH - DAMAGE_AMOUNT, life.getCurrentHealth());
            Assert.AreEqual(INITIAL_HEALTH, life.getHealth());
            life.addHealth(RECOVER_AMOUNT);
            Assert.AreEqual(INITIAL_HEALTH - DAMAGE_AMOUNT + RECOVER_AMOUNT, life.getCurrentHealth());
            Assert.AreEqual(INITIAL_HEALTH, life.getHealth());
            life.addHealth(RECOVER_AMOUNT);
            Assert.AreEqual(INITIAL_HEALTH, life.getCurrentHealth());
            Assert.AreEqual(INITIAL_HEALTH, life.getHealth());
        }
        [TestMethod]
        public void lifeTest()
        {
            ILife life = LifeImpl.createCustomizedLife(INITIAL_LIVES, INITIAL_HEALTH);
            life.loseHealth(DAMAGE_AMOUNT);
            Assert.AreEqual(INITIAL_LIVES, life.getLives());
            life.addHealth(RECOVER_AMOUNT);
            Assert.AreEqual(INITIAL_LIVES, life.getLives());

            life.loseLife();
            Assert.AreEqual(INITIAL_LIVES - 1, life.getLives());
            life.addLife();
            Assert.AreEqual(INITIAL_LIVES, life.getLives());

            life.loseHealth(life.getHealth());
            Assert.AreEqual(INITIAL_LIVES - 1, life.getLives());
            Assert.AreEqual(INITIAL_HEALTH, life.getCurrentHealth());
            life.loseHealth(life.getCurrentHealth());
            Assert.AreEqual(INITIAL_LIVES - 1 - 1, life.getLives());
            Assert.AreEqual(INITIAL_HEALTH, life.getCurrentHealth());
            life.loseLife();
            Assert.AreEqual(0, life.getLives());
            Assert.AreEqual(0, life.getCurrentHealth());
        }
    }
}
