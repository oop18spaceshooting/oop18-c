﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.CompilerServices;
using System.Threading;

namespace SpaceShooting
{
    class LifeImpl : ILife
    {
        private static int DEFAULT_HEALTH = 1000;
        private static int DEFAULT_LIVES = 1;
        private int health;
        private int lives;
        private int currentHealth;

        private LifeImpl(int lives, int startingHealth)
        {
            this.lives = lives;
            this.currentHealth = startingHealth;
            this.health = startingHealth;
        }

        public static ILife createDefaultLife()
        {
            return new LifeImpl(DEFAULT_LIVES, DEFAULT_HEALTH);
        }

        public static ILife createLifeFromStartingHealth(int startingHealth)
        {
            return new LifeImpl(DEFAULT_LIVES, startingHealth);
        }

        public static ILife createLifeWithMoreLives(int lives)
        {
            return new LifeImpl(lives, DEFAULT_HEALTH);
        }

        public static ILife createCustomizedLife(int lives, int startingHealth)
        {
            return new LifeImpl(lives, startingHealth);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void loseHealth(int health)
        {
            if (health < 0)
            {
                throw new ArgumentException();
            }
            this.currentHealth -= health;
            if (this.currentHealth <= 0)
            {
                loseLife();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void addHealth(int health)
        {
            if (health < 0)
            {
                throw new ArgumentException();
            }
            this.currentHealth += health;
            if (this.currentHealth >= this.health)
            {
                addLife();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void addLife()
        {
            this.lives++;
            this.currentHealth = this.health;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void loseLife()
        {
            this.lives--;
            if (this.lives > 0)
            {
                this.currentHealth = this.health;
            }
            else
            {
                this.currentHealth = 0;
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public int getCurrentHealth()
        {
            return this.currentHealth;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public int getLives()
        {
            return this.lives;
        }

        public int getHealth()
        {
            return this.health;
        }
    }
}
