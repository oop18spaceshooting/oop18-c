﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.CompilerServices;
using System.Threading;

namespace SpaceShooting
{
    public class ScoreImpl : IScore
    {
        private int score;

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void addScore(int value)
        {
            if (value < 0)
            {
                throw new ArgumentException("Only Positive Numbers");
            }
            this.score += value;
        }
        [MethodImpl(MethodImplOptions.Synchronized)]
        public int getScorePoints()
        {
            return this.score;
        }
    }
}
